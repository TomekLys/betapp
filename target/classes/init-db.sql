create sequence user_seq;
INSERT INTO user(id, login, password, full_name, role) values (nextval('user_seq'), 'login', 'login', 'John Example', 'ROLE_USER');
INSERT INTO user(id, login, password, full_name, role) values (nextval('user_seq'), 'admin', 'admin', 'John Admin', 'ROLE_ADMIN');

create sequence team_seq;
INSERT INTO team(id, name) values (nextval('team_seq'), 'Polska');
INSERT INTO team(id, name) values (nextval('team_seq'), 'Niemcy');

create sequence match_seq;
INSERT INTO match(id, host_team_id, guest_team_id, host_score, guest_score, match_time) values (nextval('match_seq'), 1, 2, null, null, to_timestamp('16-06-2016 21:00', 'DD-MM-YYYY HH:mm'));

commit;