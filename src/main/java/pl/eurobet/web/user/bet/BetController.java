package pl.eurobet.web.user.bet;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pl.eurobet.core.bet.Bet;
import pl.eurobet.core.match.MatchDao;

@Controller
public class BetController {
    @Autowired
    private MatchDao matchDao;

    @RequestMapping(value = "/user/bet", method = RequestMethod.GET)
    public ModelAndView betMatch(@RequestParam Long matchid){
        ModelAndView modelAndView = new ModelAndView("bet/bet");
        modelAndView.addObject("match", matchDao.getById(matchid));
        modelAndView.addObject("bet", new Bet());
        return modelAndView;
    }

    @RequestMapping(value = "/user/bet", method = RequestMethod.POST)
    public void setBet(@ModelAttribute Bet bet){
        System.out.println(bet.getHostScore());

    }
}
