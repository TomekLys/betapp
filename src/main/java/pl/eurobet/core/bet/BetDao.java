package pl.eurobet.core.bet;

import pl.eurobet.core.BaseDao;
import pl.eurobet.core.team.Team;

public interface BetDao extends BaseDao<Team, Long> {
}
