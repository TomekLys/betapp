package pl.eurobet.core.bet;

import pl.eurobet.core.match.Match;
import pl.eurobet.core.user.User;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Bet {

    @Id
    @GeneratedValue(generator = "betSeq")
    @SequenceGenerator(name = "betSeq", sequenceName = "bet_seq")
    private Long id;

    @ManyToOne
    @JoinColumn(name ="user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name="match_id")
    private Match match;

    @Column
    private Long guestScore;

    @Column
    private Long hostScore;

    @Column
    private Date betTime;

    @Column
    private Long points;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Match getMatch() {
        return match;
    }

    public void setMatch(Match match) {
        this.match = match;
    }

    public Long getGuestScore() {
        return guestScore;
    }

    public void setGuestScore(Long guestScore) {
        this.guestScore = guestScore;
    }

    public Long getHostScore() {
        return hostScore;
    }

    public void setHostScore(Long hostScore) {
        this.hostScore = hostScore;
    }

    public Date getBetTime() {
        return betTime;
    }

    public void setBetTime(Date betTime) {
        this.betTime = betTime;
    }

    public Long getPoints() {
        return points;
    }

    public void setPoints(Long points) {
        this.points = points;
    }
}
