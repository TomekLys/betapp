package pl.eurobet.core.match;

import java.util.Date;
import java.util.List;

import javax.persistence.*;

import pl.eurobet.core.bet.Bet;
import pl.eurobet.core.team.Team;

@Entity
public class Match {
	
	@Id
	@GeneratedValue(generator = "matchSeq")
	@SequenceGenerator(name = "matchSeq", sequenceName = "match_seq")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "host_team_id")
	private Team hostTeam;
	
	@ManyToOne
	@JoinColumn(name = "guest_team_id")
	private Team guestTeam;

	@OneToMany(mappedBy = "match")
	private List<Bet>betList;

	@Column
	private Long hostScore;
	
	@Column
	private Long guestScore;
	
	@Column
	private Date matchTime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Team getHostTeam() {
		return hostTeam;
	}

	public void setHostTeam(Team hostTeam) {
		this.hostTeam = hostTeam;
	}

	public Team getGuestTeam() {
		return guestTeam;
	}

	public void setGuestTeam(Team guestTeam) {
		this.guestTeam = guestTeam;
	}

	public Long getHostScore() {
		return hostScore;
	}

	public void setHostScore(Long hostScore) {
		this.hostScore = hostScore;
	}

	public Long getGuestScore() {
		return guestScore;
	}

	public void setGuestScore(Long guestScore) {
		this.guestScore = guestScore;
	}

	public Date getMatchTime() {
		return matchTime;
	}

	public void setMatchTime(Date matchTime) {
		this.matchTime = matchTime;
	}
	
	

}
