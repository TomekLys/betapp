package pl.eurobet.core;

import java.io.Serializable;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

@Repository
public abstract class AbstractBaseDao<E, K extends Serializable> extends HibernateDaoSupport implements BaseDao<E, K> {
		
	@Autowired
	public void setSessionFactoryBean(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}
	
	public List<E> getAll() {
		return getHibernateTemplate().loadAll(supports());		
	}
	
	public E getById(K key) {
		return getHibernateTemplate().get(supports(), key);	
	}

	public void delete(E entity) {
		getHibernateTemplate().delete(entity);	
	}

	public void saveOrUpdate(E entity) {
		getHibernateTemplate().saveOrUpdate(entity);	
	}
	
	abstract protected Class<E> supports();

}
