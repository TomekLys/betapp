<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">
<body>
${match.matchTime}

<form:form name='betForm'
			action="<c:url value='/user/bet' />" method='POST' modelAttribute="bet">
			<table>
				<tr>
					<td>${match.hostTeam.name}</td>
					<td><form:input type='number' path='hostScore'></td>
				</tr>
				<tr>
                	<td>${match.guestTeam.name}</td>
                	<td><form:input type='number' path='guestScore'></td>
                </tr>
				<tr>
					<td colspan='2'><input name="submit" type="submit"
						value="submit" /></td>
				</tr>
			</table>
</form>
</body>
</html>