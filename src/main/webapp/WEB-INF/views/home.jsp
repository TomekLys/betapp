<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<body>
	<c:url value="/user/matches" var="matchesUrl" />
	<c:url value="/logout" var="logoutUrl" />
	<c:url value="/admin/teams" var="teamsUrl" />
	
	<h2>Hello : ${username} !</h2>	
	<h3>Choose action: </h3>
	
	<ul>
		<sec:authorize access="hasRole('ROLE_ADMIN')">
			<li><a href="${teamsUrl}">Manage teams</a></li>
		</sec:authorize>	
		<li><a href="${matchesUrl}">Show matches</a></li>
	</ul>
	
	<form action="${logoutUrl}" method="post" id="logoutForm">
		<input type="submit" value="Logout"></input>	
	</form>

</body>
</html>