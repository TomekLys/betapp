<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">
<body>
	<c:url value="/user/bet" var="betUrl" />
	<table>
		<c:forEach var="match" items="${matches}">
			<a href="${betUrl}?matchid=${match.id}">${match.matchTime}${match.hostTeam.name}${match.guestTeam.name}</a>
		</c:forEach>
	</table>

</body>
</html>